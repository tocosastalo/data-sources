# First, specify the base Docker image. You can read more about
# the available images at https://sdk.apify.com/docs/guides/docker-images
# You can also use any other image from Docker Hub.
FROM apify/actor-node-puppeteer-chrome:16

USER root

# RUN apt-get update -y -q
# RUN apt-get install -y -q xvfb libgtk2.0-0 libxtst6 libxss1 libgconf-2-4 libnss3 libasound2

# USER node

WORKDIR /home/node/apify-crawlers

# Second, copy just package.json and package-lock.json since it should be
# the only file that affects "npm install" in the next step, to speed up the build
COPY package*.json ./

RUN npm install pm2@latest -g

# Install NPM packages.
RUN npm install --include=dev

# Next, copy the remaining files and directories with the source code.
# Since we do this after NPM install, quick build will be really fast
# for most source file changes.
COPY . ./

# ENV APIFY_LOCAL_STORAGE_DIR=apify_storage
ENV NODE_ENV=production
ENV APIFY_MEMORY_MBYTES=13000

# RUN mkdir -p /home/node/apify-crawlers/${APIFY_LOCAL_STORAGE_DIR}
# RUN chmod -R 777 /home/node/apify-crawlers/${APIFY_LOCAL_STORAGE_DIR}


RUN npm run build

# Optionally, specify how to launch the source code of your actor.
# By default, Apify's base Docker images define the CMD instruction
# that runs the Node.js source code using the command specified
# in the "scripts.start" section of the package.json file.
# In short, the instruction looks something like this:
#
CMD npm run start
# CMD ["pm2-runtime", "build/main.js", "-i", " max"]
# CMD ["pm2-runtime", "ecosystem.config.js"]
