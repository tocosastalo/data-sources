import * as dotenv from 'dotenv';
import { MongoDatabase } from './database/mongo';

const exportDatabase = async () => {
  dotenv.config();
  const mongoDb = new MongoDatabase();
  await mongoDb.initConnection();
  await mongoDb.exportDb();
};

exportDatabase();
