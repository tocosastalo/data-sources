import Axios, { AxiosInstance, AxiosProxyConfig } from 'axios';
import * as xmlConvert from 'xml-js';
import * as tunnel from 'tunnel';
import * as qs from 'qs';
import { MongoDatabase } from '../database/mongo';

export class ProQuest {
  private axios: AxiosInstance;
  private mongoDb: MongoDatabase | undefined;

  constructor(mongoDb: MongoDatabase | undefined, proxy: AxiosProxyConfig | undefined) {
    this.axios = Axios.create({
      baseURL: 'http://fedsearch.proquest.com/search/sru/all_subscribed',
      params: { version: '1.2', operation: 'searchRetrieve' },
      paramsSerializer: params => qs.stringify(params, { encode: false }),
      httpAgent:
        proxy != null
          ? tunnel.httpOverHttp({
              proxy,
            })
          : undefined,
    });
    this.mongoDb = mongoDb;
  }

  protected setMongoDb(mongoDb: MongoDatabase | undefined) {
    if (mongoDb != null) {
      this.mongoDb = mongoDb;
    } else {
      console.log(`[ProQuest] Running without connection to mongo DB.`);
    }
  }

  public async fetchData() {
    const query = this.createSearchQuery();
    console.log(query);
    try {
      const response = await this.axios.get('', {
        params: {
          maximumRecords: '3',
          startRecord: '1',
          query,
        },
        // timeout: 4000,
      });
      const converted = xmlConvert.xml2json(response.data);
      console.log('OK', converted);
    } catch (error) {
      console.error(error);
    }
  }

  private createSearchQuery() {
    const sourcesList = [
      'Los Angeles Times',
      'USA Today',
      'Chicago Tribune',
      'Washington Post',
      'Boston Globe',
      'Wall Street Journal',
      'Miami Herald',
      'Dallas Morning News',
      'Houston Chronicle',
      'San Francisco Chronicle',
      'New York Times',
    ];

    const sources = `dc.source=${sourcesList.join(' or ').replace(/ /g, '+')}`;
    const language = `dc.language=English`;

    return `${sources}`;
  }
}

/**
 * PUB(Los Angeles Times or USA Today or Chicago Tribune or Washington Post or Boston Globe or Wall Street Journal or Miami Herald or Dallas Morning News or Houston Chronicle or San Francisco Chronicle or New York Times) 
 * and STYPE(newspapers) and LA(english)
Ešte sa tam ale dá nastaviť v ProQueste cez checkbox "full text" a "Exclude duplicate documents". 
 Exclude duplicates v podstate znamená, že ak to oni ťahajú z viacerých databázii môže sa tam objaviť daný článok aj 2x.
 */
