import Apify from 'apify';
import { Collection, Db, MongoClient } from 'mongodb';
import { jsonStreamWrite } from '../helpers/other';
import { MongoCredentials, MongoRecord } from '../types';

const {
  utils: { log },
} = Apify;

export class MongoDatabase {
  private client: MongoClient;
  private db: Db;
  private collection: Collection<MongoRecord>;

  public async initConnection(credentials?: Partial<MongoCredentials>) {
    const { connectionUrl, collectionName } = this.getConnectionMetadata(credentials);
    log.info(`[MongoDB] Connecting to database...`, { connectionUrl });

    try {
      this.client = await MongoClient.connect(connectionUrl, { ignoreUndefined: true });
      this.db = this.client.db();
      this.collection = this.db.collection(collectionName);
      log.info(`[MongoDB] Connection with database is initialized.`, {
        usedCollectionName: this.collection.collectionName,
      });
    } catch (error) {
      const m = `[MongoDB] Connection to database was not initialized.`;
      log.exception(error, m);
      throw new Error(m);
    }
  }

  public async closeClient() {
    await this.client.close();
    log.info(`[MongoDB] Connection with database is automatically closed.`);
  }

  public async insertOneRecord(record: MongoRecord) {
    try {
      await this.collection.insertOne(record);
    } catch (error) {
      log.exception(error, '[MongoDB] insertOneRecord error.', { record });
    }
  }

  public getCollection() {
    return this.collection;
  }

  private getConnectionMetadata(credentials?: Partial<MongoCredentials>) {
    const { MONGO_USERNAME, MONGO_PASSWORD, MONGO_HOSTNAME, MONGO_PORT, MONGO_DB, MONGO_COLLECTION } = process.env;

    credentials = {
      MONGO_USERNAME,
      MONGO_PASSWORD,
      MONGO_HOSTNAME,
      MONGO_PORT,
      MONGO_DB,
      MONGO_COLLECTION,
      ...credentials,
    };

    const missingCredentials = Object.keys(credentials).filter(key => credentials[key] == null);
    if (missingCredentials.length) {
      throw new Error(`[MongoDB] Following credentials were not set: ${missingCredentials.join(', ')}.`);
    }

    const connectionUrl = `mongodb://${credentials.MONGO_USERNAME}:${credentials.MONGO_PASSWORD}@${credentials.MONGO_HOSTNAME}:${credentials.MONGO_PORT}/${credentials.MONGO_DB}?authSource=admin`;

    return {
      connectionUrl,
      collectionName: credentials.MONGO_COLLECTION,
    };
  }

  public async exportDb() {
    console.log('Export start');

    const data = await this.getCollection().find().toArray();
    console.log('Data fetched from DB');

    jsonStreamWrite('exported.json', data);
  }
}
