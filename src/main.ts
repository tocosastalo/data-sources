import Apify from 'apify';
import * as dotenv from 'dotenv';
import { ProquestRunner } from './crawlers/proquest/proquestRunner';
import { MongoDatabase } from './database/mongo';

if (process.env.NODE_ENV !== 'production') {
  // init config from local .env
  console.log('Loading local .env file...');
  dotenv.config();
}

const initMongoDb = async () => {
  if (process.env.DISABLE_MONGO !== 'true') {
    const mongoDb = new MongoDatabase();
    await mongoDb.initConnection();
    return mongoDb;
  }
};

const runCrawlers = async () => {
  const mongoDb = await initMongoDb();

  Apify.main(async () => {
    const runner = new ProquestRunner(mongoDb);

    await runner.run({
      query:
        'PUB(Los Angeles Times or USA Today or Chicago Tribune or Washington Post or Boston Globe or Wall Street Journal or Miami Herald or Dallas Morning News or Houston Chronicle or San Francisco Chronicle or New York Times) and STYPE(newspapers) and LA(english)',
      fullText: true,
    });

    if (mongoDb != null) {
      mongoDb.closeClient();
    }
  });
};

runCrawlers();
