import { ElementHandle, Page } from 'puppeteer';

export const writeTextToInput = async (text: string, page: Page, selector?: string, elm?: ElementHandle<Element>) => {
  if (selector != null) {
    await page.focus(selector);
  }

  if (elm != null) {
    await elm.focus();
  }

  await page.keyboard.down('Control');
  await page.keyboard.press('A');
  await page.keyboard.up('Control');
  await page.keyboard.press('Backspace');
  await page.keyboard.type(text);
};

export const getElementText = async (elm: ElementHandle<Element>): Promise<string> => {
  const jsHandle = await elm.getProperty('innerText');
  const elmText: string = await jsHandle.jsonValue();

  return elmText;
};
