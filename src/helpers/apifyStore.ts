import Apify from 'apify';

export class ApifyStore {
  private value: string;
  private name: string;

  constructor(name: string, value: string) {
    this.name = name;
    this.value = value;
  }

  private async openStore() {
    return await Apify.openKeyValueStore(this.name);
  }

  async getValue<T>(): Promise<T> {
    const store = await this.openStore();
    return (await store.getValue(this.value)) as Promise<T>;
  }

  async setValue(val: Apify.KeyValueStoreValueTypes) {
    const store = await this.openStore();
    await store.setValue(this.value, val);
  }

  async drop() {
    const store = await this.openStore();
    await store.drop();
  }
}
