import * as JSONStream from 'JSONStream';
import { MongoRecord } from '../types';
const fs = require('fs');

export function jsonStreamWrite(filepath: string, records: MongoRecord[]) {
  const transformStream = JSONStream.stringify();
  const outputStream = fs.createWriteStream(filepath);

  transformStream.pipe(outputStream);

  let published_at = null;
  records.forEach((record, i) => {
    published_at = record.published_at = record.published_at ?? published_at;

    if (published_at == null) {
      console.error('Not published at');
    }
    
    transformStream.write(record);
  });
  transformStream.end();

  outputStream.on('finish', function handleFinish() {
    console.log('JSONStream serialization complete!');
  });
}
