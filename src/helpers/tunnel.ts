import { Server } from 'net';
import * as tunnel from 'tunnel-ssh';

export const createAkelaTunnel = () => {
  const config: tunnel.Config = {
    username: 'xjakubek',
    password: 'x',
    host: '195.113.194.135',
    port: 22,
    dstHost: 'localhost',
    dstPort: 8080,
    // localHost: '127.0.0.1',
    // localPort: 27018,
  };

  return new Promise<Server>((resolve, reject) => {
    const server = tunnel(config, (error, server) => {
      if (error) {
        console.log('SSH connection error: ' + error);
        return reject(error);
      }
      resolve(server);
    });
  });
};
