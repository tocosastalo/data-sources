export const FED_SEARCH_API_BASE = 'http://fedsearch.proquest.com/search';
export const DATE_FORMAT = 'yyyy-MM-dd';

export const RESULTS_LIMIT = Number(process.env.RESULTS_LIMIT ?? 10000);
export const DEFAULT_DATE_RANGE = Number(process.env.DEFAULT_DATE_RANGE ?? 9);
export const PAGES_BUFFER = Number(process.env.PAGES_BUFFER ?? 50); // 50000
export const ITEMS_PER_PAGE = Number(process.env.ITEMS_PER_PAGE ?? 100);
export const TURN_ON_RESULTS_CRAWLER = process.env.TURN_ON_RESULTS_CRAWLER === 'true';
export const TURN_ON_DETAILS_CRAWLER = process.env.TURN_ON_DETAILS_CRAWLER === 'true';
export const DETAILS_CRAWLER_INSTANCES = Number(process.env.DETAILS_CRAWLER_INSTANCES ?? 7);
export const MIN_CONCURRENCY = Number(process.env.MIN_CONCURRENCY ?? 6);
export const MAX_CONCURRENCY = Number(process.env.MAX_CONCURRENCY ?? 25);
export const DATE_RANGE_START = process.env.DATE_RANGE_START;
