import Apify, {
  BrowserCrawlingContext,
  CrawlingContext,
  HandleFailedRequestInput,
  PuppeteerCrawler,
  PuppeteerCrawlerOptions,
  PuppeteerLaunchContext,
  Session,
  SessionPoolOptions,
} from 'apify';
import { Page, PuppeteerNodeLaunchOptions } from 'puppeteer';
import { ApifyStore } from '../helpers/apifyStore';
import { MAX_CONCURRENCY, MIN_CONCURRENCY } from '../helpers/constants';
import { getElementText, writeTextToInput } from '../helpers/pup';
import { PuppeteerPageContext } from '../types';

const {
  utils: { log },
} = Apify;

export abstract class CrawlerBase {
  protected crawler: Apify.PuppeteerCrawler;
  protected requestQueue: Apify.RequestQueue;
  protected crawlerName: string;
  protected loginPromise: Promise<any> | undefined;
  private cookieStore: ApifyStore;
  private launchOptions: PuppeteerNodeLaunchOptions;
  private homepageUrl: string;

  constructor(cookieStore: ApifyStore, crawlerName: string) {
    this.cookieStore = cookieStore;
    this.homepageUrl = 'https://www.proquest.com/?accountid=28016';
    this.crawlerName = crawlerName;

    this.launchOptions = {
      headless: true,
      ignoreHTTPSErrors: true,
      args: [
        '--no-sandbox',
        '--disable-setuid-sandbox',
        '--disable-extensions',
        '--disable-dev-shm-usage',
        '--disable-web-security',
        '--disable-accelerated-2d-canvas',
        '--disable-canvas-aa',
        '--disable-2d-canvas-clip-aa',
        '--disable-gl-drawing-for-tests',
        '--disable-gpu',
        '--disable-features=IsolateOrigins,site-per-process',
        '--mute-audio',
        '--disable-infobars',
        '--disable-breakpad',
        '--shm-size=13gb',
        '--ignore-certificate-errors',
        '--ignore-certificate-errors-spki-list',
      ],
    };
  }

  public async initPuppeteerCrawler(options: PuppeteerCrawlerOptions) {
    this.crawler = new Apify.PuppeteerCrawler({
      launchContext: {
        launchOptions: { ...this.launchOptions },
        stealth: false,
        useChrome: false,
      } as PuppeteerLaunchContext,
      maxRequestRetries: 20,
      navigationTimeoutSecs: 50,
      handlePageTimeoutSecs: 90,
      persistCookiesPerSession: true,
      useSessionPool: true,
      sessionPoolOptions: this.getSessionPoolOptions(),
      handleFailedRequestFunction: this.handleFailedRequestFunction,
      requestQueue: this.requestQueue,
      minConcurrency: MIN_CONCURRENCY,
      maxConcurrency: MAX_CONCURRENCY,
      ...options,
    });
  }

  public async run() {
    if (this.crawler == null) {
      throw new Error(`[${this.crawlerName}] Crawler is not well initialized. First call 'initCrawler()'.`);
    }

    log.info(`[${this.crawlerName}] Starting crawler...`);

    await this.crawler.run();

    log.info(`[${this.crawlerName}] Crawler finished his excellent job.`);
  }

  protected async handleExpiredSession(
    context: CrawlingContext &
      BrowserCrawlingContext & {
        page: Page;
        crawler: PuppeteerCrawler;
      },
  ) {
    const { page, session } = context;

    await this.handleIntermediateRedirectProxy(page);

    const sessionExpired = page.url().includes('sessionexpired');
    const captchaBlock = await page.$('#verifyCaptcha');

    if (captchaBlock == null && !sessionExpired) {
      return false;
    }

    if (this.loginPromise == null) {
      log.info(`[${this.crawlerName}] Captcha or session expired! Re-login...`, {
        ...this.createDefaultContextLog(context),
      });

      this.loginPromise = this.login(true, true);
    }

    await this.loginPromise;
    this.loginPromise = undefined;

    if (!page.isClosed()) {
      const userCookies = await this.cookieStore.getValue<[]>();
      await page.setCookie(...userCookies);
      await this.loadSession(session);
      await page.reload();
      await this.handleIntermediateRedirectProxy(page);
    }
  }

  private async loadSession(session: Session) {
    const userCookies = await this.cookieStore.getValue<any[]>();

    if (userCookies != null && Array.isArray(userCookies)) {
      session.setPuppeteerCookies(
        userCookies.map(cook => {
          return { ...cook };
        }),
        'https://www.proquest.com/',
      );
    }

    return session;
  }

  protected async login(forceNewCookies = false, closeBrowser = false) {
    log.info(`[${this.crawlerName}] Login to ProQuest...`);

    const { browser, page } = await this.openBrowserPage();

    const userCookies = forceNewCookies ? undefined : await this.cookieStore.getValue<[]>();

    if (userCookies != null) {
      log.info(`[${this.crawlerName}] Trying to use cached cookies.`);
      await page.setCookie(...userCookies);

      await page.goto(this.homepageUrl);
      await this.handleIntermediateRedirectProxy(page);

      const isLogged = await this.loggedCheck(page);

      if (!isLogged) {
        log.info(`[${this.crawlerName}] Cookies didn't work. Dropping cookie store and repeat.`);
        await this.cookieStore.drop();
        await browser.close();
        await this.login();
      }
    } else {
      log.info(`[${this.crawlerName}] ${forceNewCookies ? 'Forced new cookies.' : 'No cached cookies found.'}`);

      await page.goto(this.homepageUrl);
      await this.handleIntermediateRedirectProxy(page);

      const isLogged = await this.loggedCheck(page);
      if (!isLogged) {
        await this.shibbolethLogin(page);
      } else {
        log.info(`[${this.crawlerName}] Shibboleth login skipped (we are on the Mendelu network).`);
      }
    }

    log.info(`[${this.crawlerName}] ProQuest login successful. Refreshing cookies in cache.`);
    const cookies = await page.cookies();
    await this.cookieStore.setValue(cookies);

    if (closeBrowser) {
      await browser.close();
    }

    return { page, browser };
  }

  protected async openBrowserPage() {
    const browser = await Apify.launchPuppeteer({ launchOptions: { ...this.launchOptions } });
    const page: Page = await browser.newPage();
    return { browser, page };
  }

  private async shibbolethLogin(page: Page) {
    log.info(`[${this.crawlerName}] Shibboleth login in progress...`, { url: page.url() });

    const input = {
      username: process.env.X_LOGIN,
      password: process.env.X_PASS,
    };

    if (input.username == null || input.password == null) {
      throw new Error(`[${this.crawlerName}] Missing login credentials. Did you set 'X_LOGIN' and 'X_PASS'?`);
    }

    if (!page.url().includes('idp.mendelu')) {
      await page.goto(this.homepageUrl);
      await this.handleIntermediateRedirectProxy(page);
    }

    await writeTextToInput(input.username, page, '#username');
    await writeTextToInput(input.password, page, '#password');
    await page.click('form button[type="submit"]');

    const selector = 'input[type="submit"]:nth-child(2)';
    await page.waitForSelector(selector, { timeout: 10000 });

    await Promise.all([page.waitForNavigation(), page.click(selector)]);

    const isLogged = await this.loggedCheck(page);

    if (!isLogged) {
      throw new Error(`[${this.crawlerName}] Incorrect username or password.`);
    }
  }

  private loggedCheck = async (page: Page) => {
    try {
      await this.handleIntermediateRedirectProxy(page);

      const url = page.url();
      if (url.includes('idp.mendelu')) {
        return false;
      }

      if (url.includes('proquest.com')) {
        const elm = await page.waitForSelector('#institution-name', { timeout: 8000 });
        const elmText = await getElementText(elm);

        return elmText === 'Mendelova univerzita v Brne';
      }

      return false;
    } catch (err) {
      console.error(err);
      return false;
    }
  };

  private async handleIntermediateRedirectProxy(page: Page) {
    await page.waitForNetworkIdle();

    const intermediateredirectforezproxy = page.url().includes('intermediateredirectforezproxy');
    if (intermediateredirectforezproxy) {
      try {
        await page.waitForNavigation({ timeout: 10000 });
      } catch (error) {}
      return this.handleIntermediateRedirectProxy(page);
    }
    return true;
  }

  private getSessionPoolOptions(): SessionPoolOptions {
    return {
      createSessionFunction: async sessionPool => {
        const session = new Apify.Session({
          sessionPool,
        });
        return await this.loadSession(session);
      },
      maxPoolSize: 1000000000000000,
    };
  }

  private handleFailedRequestFunction: Apify.HandleFailedRequest = async context => {
    const { message } = context.error;

    log.warning(`Request failed too many times.`, {
      ...this.createDefaultContextLog(context),
      error: message,
    });
  };

  protected createDefaultContextLog({ request: { url }, proxyInfo }: PuppeteerPageContext | HandleFailedRequestInput) {
    return {
      url,
      ...(proxyInfo != null ? { sessionId: proxyInfo.sessionId, proxyUrl: proxyInfo.url } : {}),
    };
  }

  public getCrawler() {
    return this.crawler;
  }
}
