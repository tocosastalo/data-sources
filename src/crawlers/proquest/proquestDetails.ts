import Apify from 'apify';
import { Page } from 'puppeteer';
import { ApifyStore } from '../../helpers/apifyStore';
import { MongoRecord, PuppeteerPageContext } from '../../types';
import { CrawlerBase } from '../CrawlerBase';

const {
  utils: { log },
} = Apify;

export class ProquestCrawlerDetails extends CrawlerBase {
  private onNewRecord: (record: MongoRecord) => void;

  constructor(onNewRecord: (record: MongoRecord) => void, id: number) {
    super(new ApifyStore('cookies', `details_${id}`), `Details_crawler_${id}`);
    this.onNewRecord = onNewRecord;
  }

  public async initCrawler(que: Apify.RequestQueue) {
    log.info(`[${this.crawlerName}] Initialize crawler...`);

    const { browser } = await this.login(true);
    await browser.close();

    this.requestQueue = que;

    await this.initPuppeteerCrawler({
      handlePageFunction: this.handlePageFunction,
    });
  }

  private handlePageFunction: Apify.PuppeteerHandlePage = async context => {
    let wasProcessed = false;

    const i = setInterval(async () => {
      if (!wasProcessed && this.loginPromise == null) {
        // log.info(`[${this.crawlerName}] Forcing page processing.`, this.createDefaultContextLog(context));
        if (!context.page.isClosed()) {
          await context.page.bringToFront();
          // clearInterval(i);
        }
      }
    }, 5000 + Math.floor(Math.random() * (1200 - 50 + 1)) + 50);

    await this.handleExpiredSession(context);

    await this.handleResultDetailPage(context);

    wasProcessed = true;
    clearInterval(i);
  };

  private handleResultDetailPage: Apify.PuppeteerHandlePage = async context => {
    log.info(`[${this.crawlerName}] Processing page.`, this.createDefaultContextLog(context));

    const mongoRecord = await this.scrapePageData(context);
    if (mongoRecord != null && Object.keys(mongoRecord).includes('full_text')) {
      this.onNewRecord(mongoRecord);
    }
  };

  private async scrapePageData(context: PuppeteerPageContext) {
    const { page } = context;

    const waitForPageActive = async (page: Page) => {
      const promises = [
        page.waitForSelector('#documentTitle', { timeout: 10000 }),
        page.waitForSelector('#readableContent text', { timeout: 11000 }),
      ];

      try {
        await Promise.all(promises);
        return true;
      } catch (error) {
        await page.bringToFront();
        return waitForPageActive(page);
      }
    };

    await waitForPageActive(page);

    const record: MongoRecord = {} as MongoRecord;
    record.proquest_id = this.parseProquestIdFromUrl(page.url());
    record.retrieved_at = new Date();
    record.abstract = await page.$$eval('.abstractContainer .abstract', els => els[0]?.textContent?.trim());

    try {
      record.article_title = await page.$eval('#documentTitle', el => el.textContent.trim());
      record.full_text = await page.$eval('#readableContent text', el => el.textContent.trim());
    } catch (error) {
      const m = `[${this.crawlerName}] Some of required data was not scraped from page. ${error.message}`;
      throw new Error(m);
    }

    try {
      await page.click('article .tabs li:nth-child(2) a');
      await page.waitForSelector('.display_record_indexing_fieldname');

      const rows = await page.$$eval('.display_record_indexing_fieldname', els =>
        els
          .filter(el => el.textContent != null)
          .map(el => ({ textContent: el.textContent, nextSiblingTextContent: el.nextElementSibling?.textContent })),
      );

      const getTableValue = (header: string) =>
        rows.find(el => el.textContent.trim() === header)?.nextSiblingTextContent || undefined;

      record.authors = getTableValue('Author');
      record.published_at = getTableValue('Publication date');
      record.source_type = getTableValue('Source type');
      record.lang = getTableValue('Language of publication');
      record.publisher = getTableValue('Publisher');
      record.document_type = getTableValue('Document type');
      record.url = getTableValue('Document URL');
    } catch (error) {}

    if (record.published_at == null) {
      throw new Error(`[${this.crawlerName}] Required attribute 'published_at' was not scrapped. URL: ${page.url()}`);
    }

    return record;
  }

  private parseProquestIdFromUrl(url: string) {
    const match = url.match(/\d+/);
    if (match && match.length) {
      return match[0];
    }
    return url;
  }
}
