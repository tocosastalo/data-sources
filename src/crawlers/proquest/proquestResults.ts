import Apify from 'apify';
import { format, subDays } from 'date-fns';
import { Page } from 'puppeteer';
import { ApifyStore } from '../../helpers/apifyStore';
import { DATE_FORMAT, DATE_RANGE_START, DEFAULT_DATE_RANGE, ITEMS_PER_PAGE, RESULTS_LIMIT } from '../../helpers/constants';
import { writeTextToInput } from '../../helpers/pup';
import { NewDoc } from '../../types';
import { CrawlerBase } from '../CrawlerBase';
import { SearchQuery } from './types';

const {
  utils: { log },
} = Apify;

export class ProquestResultsCrawler extends CrawlerBase {
  private dateRange: {
    start: Date | undefined;
    end: Date | undefined;
    tempStart: Date | undefined;
    tempEnd: Date | undefined;
    len: number;
  };
  private haveMoreResults: boolean;
  private onNewDocLinks: (docs: NewDoc[]) => void;

  constructor(onNewDocLinks: (docs: NewDoc[]) => void) {
    super(new ApifyStore('cookies', 'results'), 'Results_crawler');
    const start = DATE_RANGE_START != null ? new Date(DATE_RANGE_START) : undefined;
    this.dateRange = {
      start: start,
      end: undefined,
      tempStart: start,
      tempEnd: undefined,
      len: DEFAULT_DATE_RANGE,
    };
    this.haveMoreResults = false;
    this.onNewDocLinks = onNewDocLinks;
  }

  public async initCrawler(searchQuery: SearchQuery) {
    log.info(`[${this.crawlerName}] Initialize crawler...`);

    const { page, browser } = await this.login(true);
    await this.enterQuery(searchQuery, page);

    const url = page.url();
    await browser.close();

    this.requestQueue = await Apify.openRequestQueue((Math.random() + 1).toString(36).substring(7));
    await this.requestQueue.addRequest({ url });

    await this.initPuppeteerCrawler({
      handlePageFunction: this.handlePageFunction,
    });
  }

  private async enterQuery(searchQuery: SearchQuery, page: Page) {
    try {
      await this.handleCookiesWindow(page);
      await this.enterSearchQuery(page, searchQuery);
      log.info(`[${this.crawlerName}] Search query entered.`);
    } catch (error) {
      log.error(`[${this.crawlerName}] Enter search query error: ${error.message}`);
    }
  }

  private async enterSearchQuery(page: Page, query: SearchQuery) {
    await page.waitForSelector('#searchTerm', { timeout: 5000 });

    await writeTextToInput(query.query, page, '#searchTerm');

    if (query.fullText) {
      // @ts-expect-error
      await page.$eval('#fullTextLimit', el => (el.checked = true));
    }

    page.click('#expandedSearch');
    await page.waitForNavigation();

    return page;
  }

  private async handleCookiesWindow(page: Page) {
    log.info(`[${this.crawlerName}] Waiting for cookies popup overlay.`);

    try {
      await page.waitForNetworkIdle();
      await page.waitForSelector('.truste_box_overlay', { timeout: 5000 });

      const elementHandle = await page.$('.truste_box_overlay iframe');
      const frame = await elementHandle.contentFrame();

      await frame.waitForSelector('.pdynamicbutton a.call');
      await frame.click('.pdynamicbutton a.call');

      await frame.waitForSelector('.pdynamicbutton a.close');
      await frame.click('.pdynamicbutton a.close');

      log.info(`[${this.crawlerName}] Cookies window resolved.`);
    } catch (error) {
      log.info(`[${this.crawlerName}] Cookies window skipped.`);
    }
  }

  private async changeItemsPerPage(page: Page) {
    await page.waitForSelector('#itemsPerPage', { timeout: 500000 });

    // @ts-expect-error
    const option = await page.$eval('#itemsPerPage', node => node.value);

    if (option !== ITEMS_PER_PAGE.toString()) {
      await page.waitForNetworkIdle();

      page.select('#itemsPerPage', ITEMS_PER_PAGE.toString());
      await page.waitForNavigation();

      log.info(`[${this.crawlerName}] Items per page changed to '${ITEMS_PER_PAGE}'.`);
    }

    return page;
  }

  private handlePageFunction = async (context, forceDateChange?: boolean) => {
    const { page } = context;

    await this.handleExpiredSession(context);

    await this.handleResultsReduce(page, forceDateChange);
    if (!this.haveMoreResults) {
      return;
    }

    return this.handleResultsPage(context);
  };

  private handleResultsReduce = async (page: Page, forceDateChange = false) => {
    // ProQuest can handle only 10 000 results per query
    // if there are more results we need to decrease number of results by setting custom date range

    if (this.haveMoreResults) {
      return;
    }

    await page.waitForNetworkIdle();

    const { len, start, tempEnd, tempStart } = this.dateRange;

    if (forceDateChange === false) {
      const count = await this.getResultsCount(page);
      log.info(`[${this.crawlerName}] Current results count is '${count}' (max. '${RESULTS_LIMIT}').`);

      if (count <= RESULTS_LIMIT) {
        log.info(`[${this.crawlerName}] Date range updated.`);
        this.dateRange.start = tempStart;
        this.dateRange.end = tempEnd;
        this.dateRange.len = DEFAULT_DATE_RANGE;
        this.haveMoreResults = true;

        await this.goToFirstPage(page);
        return;
      }
    } else {
      log.info(`[${this.crawlerName}] Changing date range...`);
    }

    this.dateRange.tempStart = start != null ? subDays(start, len + 1) : subDays(new Date(), len === 0 ? 0 : len);
    this.dateRange.tempEnd = start != null ? subDays(start, 1) : new Date();
    this.dateRange.len = len === 0 ? 0 : len - 1;

    const startDateFormatted = format(this.dateRange.tempStart, DATE_FORMAT);
    const endDateFormatted = format(this.dateRange.tempEnd, DATE_FORMAT);

    const { endInput, startInput } = await this.getDateRangeInputs(page);
    await writeTextToInput(startDateFormatted, page, undefined, startInput);
    await writeTextToInput(endDateFormatted, page, undefined, endInput);

    log.info(`[${this.crawlerName}] Reducing results using date range '${startDateFormatted}'-'${endDateFormatted}'.`);

    await page.click('#dateRangeSubmit');
    await page.waitForNetworkIdle();

    if (len >= 0) {
      await this.handleResultsReduce(page);
    }
  };

  private async goToFirstPage(page: Page) {
    try {
      // const firstPageHref = await page.$eval('.pagination li a[title="First page"]', (elm: any) => elm.href);
      // await this.requestQueue.addRequest({ url: firstPageHref, userData: { label: 'RESULTS' } });

      const selector = '.pagination li a[title="First page"]';
      await page.waitForSelector(selector, { timeout: 5000 });

      await Promise.all([page.waitForNavigation(), page.click(selector)]);
    } catch (error) {}
  }

  private async getDateRangeInputs(page: Page) {
    try {
      await page.waitForSelector('#customDateRange', { visible: true, timeout: 5000 });
    } catch (error) {
      await page.click('#customDateRangeLink');
    }

    const startInput = await page.$('#startingDate');
    const endInput = await page.$('#endingDate');

    return { startInput, endInput };
  }

  private handleResultsPage: Apify.PuppeteerHandlePage = async context => {
    const { page } = context;
    log.info(`[${this.crawlerName}] Collecting document urls.`, this.createDefaultContextLog(context));

    await this.changeItemsPerPage(page);

    const newDocs: NewDoc[] = await page.$$eval('.resultHeader h3 a', elms =>
      elms.map((el: any) => ({ id: el.href.match(/\d+/)[0] ?? el.href, href: el.href })),
    );

    const links = newDocs.map(doc => ({ ...doc, href: doc.href.match(/.*\/docview\/\d*/g)[0] ?? doc.href }));

    this.onNewDocLinks(links);

    const resultsReachedElm = await page.$('.pagingBoxNoResultContent');
    if (resultsReachedElm != null) {
      log.info(`[${this.crawlerName}] Reached no-result-content box.`);
      this.haveMoreResults = false;
      await this.handlePageFunction(context, true);
      return;
    }

    try {
      const nextPageHref = await page.$eval('.pagination li a[title="Next Page"]', (elm: any) => elm.href);
      await this.requestQueue.addRequest({ url: nextPageHref });
    } catch (error) {
      this.haveMoreResults = false;
      await this.handlePageFunction(context, true);
    }
  };

  private async getResultsCount(page: Page) {
    try {
      const elmText = await page.$eval('#pqResultsCount', el => el.textContent.trim());
      const count = parseInt(elmText.replaceAll(',', ''));

      return count;
      // return isNaN(count) ? undefined : count;
    } catch (error) {
      throw new Error(`[${this.crawlerName}] Results count was not parsed. '${error.message}'`);
    }
  }
}
