import Apify from 'apify';
import { formatDistanceToNowStrict } from 'date-fns';
import { MongoDatabase } from '../../database/mongo';
import { ApifyStore } from '../../helpers/apifyStore';
import { DETAILS_CRAWLER_INSTANCES, TURN_ON_DETAILS_CRAWLER, TURN_ON_RESULTS_CRAWLER } from '../../helpers/constants';
import { MongoRecord, NewDoc } from '../../types';
import { ProquestCrawlerDetailsFactory } from './proquestDetailsFactory';
import { ProquestResultsCrawler } from './proquestResults';
import { SearchQuery } from './types';

const {
  utils: { log },
} = Apify;

export class ProquestRunner {
  private resultsCrawl: ProquestResultsCrawler;
  private detailsCrawlFactory: ProquestCrawlerDetailsFactory;
  private mongoDb: MongoDatabase | undefined;
  private newDocs: NewDoc[];
  private allDocs: NewDoc[];
  private pageCounter: number;
  private startDate: Date;

  constructor(mongoDb: MongoDatabase | undefined) {
    this.setMongoDb(mongoDb);
    this.pageCounter = 0;
    this.newDocs = [];
    this.allDocs = [];
    this.resultsCrawl = new ProquestResultsCrawler(this.addNewDocLinks);
    this.detailsCrawlFactory = new ProquestCrawlerDetailsFactory(this.processNewMongoRecord, DETAILS_CRAWLER_INSTANCES);
  }

  public async run(query: SearchQuery) {
    const sessionStore = new ApifyStore('default', 'SDK_SESSION_POOL_STATE');
    await sessionStore.drop();

    await this.detailsCrawlFactory.initQueue();

    const promises = [];

    if (TURN_ON_RESULTS_CRAWLER) {
      promises.push(this.resultsCrawl.initCrawler(query));
    }

    if (TURN_ON_DETAILS_CRAWLER) {
      promises.push(...this.detailsCrawlFactory.initCrawlersPromises());
    }

    await Promise.all(promises);

    this.startDate = new Date();
    setInterval(() => {
      log.info(`--------------------------------------------------------------`);
      log.info(`[ProquestRunner] Party time ~ ${formatDistanceToNowStrict(this.startDate)}.`);
      log.info(`--------------------------------------------------------------`);
    }, 1000 * 30);

    if (TURN_ON_RESULTS_CRAWLER) {
      setInterval(() => {
        this.transferDocs();
      }, 1000 * 30);

      return await this.resultsCrawl.run();
    }

    log.info(`[ProquestRunner] Running without results crawler.`);
    await this.detailsCrawlFactory.run();
  }

  private async transferDocs() {
    const uniqueDocs = await this.filterNewDocuments();

    if (!uniqueDocs.length) {
      return;
    }

    this.allDocs.push(...uniqueDocs);
    this.newDocs = [];

    await this.detailsCrawlFactory.queueDocuments(uniqueDocs);
    if (TURN_ON_DETAILS_CRAWLER) {
      await this.detailsCrawlFactory.run();
    }
  }

  private setMongoDb(mongoDb: MongoDatabase | undefined) {
    if (mongoDb != null) {
      this.mongoDb = mongoDb;
    } else {
      log.info(`[ProquestRunner] Running without connection to mongo DB.`);
    }
  }

  private processNewMongoRecord = (mongoRecord: MongoRecord) => {
    this.pageCounter++;

    log.info(`[ProquestRunner] Page scraped successfully (${this.pageCounter}).`, {
      // mongoRecord,
      id: mongoRecord.proquest_id,
    });

    if (this.mongoDb != null) {
      this.mongoDb.insertOneRecord(mongoRecord);
    }
  };

  private addNewDocLinks = (docs: NewDoc[]) => {
    this.newDocs.push(...docs);
    log.info(
      `[ProquestRunner] Added '${docs.length}' new document links (total: ${this.allDocs.length}, batch: ${this.newDocs.length}).`,
    );
  };

  private async filterNewDocuments() {
    if (this.mongoDb == null) {
      // mongo not used -> all received urls are new
      return [...this.newDocs];
    }

    const collection = this.mongoDb.getCollection();

    const uniqueDocs: NewDoc[] = [];
    for (const doc of this.newDocs) {
      const cursor = collection.find({ proquest_id: doc.id }, { projection: { proquest_id: 1 } });
      const existInDb = (await cursor.count()) > 0;
      const existInNews = uniqueDocs.some(d => d.id === doc.id);

      if (!existInDb && !existInNews) {
        uniqueDocs.push(doc);
      }

      cursor.close();
    }

    return uniqueDocs;
  }
}
