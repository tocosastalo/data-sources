import Apify from 'apify';
import { MongoRecord, NewDoc } from '../../types';
import { ProquestCrawlerDetails } from './proquestDetails';

const {
  utils: { log },
} = Apify;

export class ProquestCrawlerDetailsFactory {
  private detailsCrawls: ProquestCrawlerDetails[];
  private requestQueue: Apify.RequestQueue;

  constructor(onNewRecord: (record: MongoRecord) => void, count: number) {
    this.detailsCrawls = Array.from(Array(count), (_, i) => new ProquestCrawlerDetails(onNewRecord, i + 1));
  }

  public async initQueue() {
    this.requestQueue = await Apify.openRequestQueue('details');
  }

  public initCrawlersPromises() {
    setInterval(async () => {
      const { pendingRequestCount, totalRequestCount, handledRequestCount } = await this.requestQueue.getInfo();
      log.info(`[Details_crawler_factory] Details request queue info.`, {
        pendingRequestCount,
        totalRequestCount,
        handledRequestCount,
      });
    }, 1000 * 40);

    return this.detailsCrawls.map(c => c.initCrawler(this.requestQueue));
  }

  public async queueDocuments(newDocs: NewDoc[]) {
    log.info(`[Details_crawler_factory] Adding '${newDocs.length}' unique urls to details request queue.`);

    for (const doc of newDocs) {
      await this.requestQueue.addRequest({ url: doc.href });
    }
  }

  public async run() {
    const promises: Promise<void>[] = [];
    for (const c of this.detailsCrawls) {
      const detailPupCrawler = c.getCrawler();
      const isRunningPromise = await detailPupCrawler.isRunningPromise;
      if (isRunningPromise == null) {
        promises.push(c.run());
      }
    }

    await Promise.all(promises);
  }
}
