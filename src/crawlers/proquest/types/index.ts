export interface SearchQuery {
  query: string;
  fullText: boolean;
}
