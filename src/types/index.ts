import { BrowserCrawlingContext, CrawlingContext, PuppeteerCrawler } from 'apify';
import { Page } from 'puppeteer';

export interface MongoRecord {
  article_title: string;
  full_text: string;
  retrieved_at: Date;
  proquest_id: string;
  lang?: string;
  published_at?: string;
  authors?: string;
  abstract?: string;
  source_type?: string;
  publisher?: string;
  document_type?: string;
  url?: string;
}

export interface MongoCredentials {
  MONGO_USERNAME: string;
  MONGO_PASSWORD: string;
  MONGO_HOSTNAME: string;
  MONGO_PORT: string;
  MONGO_DB: string;
  MONGO_COLLECTION: string;
}

export type PuppeteerPageContext = CrawlingContext &
  BrowserCrawlingContext & {
    page: Page;
    crawler: PuppeteerCrawler;
  };

export interface NewDoc {
  href: string;
  id: string;
}
