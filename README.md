## Local development (outside Mendel network)
([source](https://medium.com/teamarimac/access-a-server-through-ssh-tunnel-and-send-api-requests-through-postman-in-windows-10-b7307974c1a0))
1. create SSH tunnel to akela.mendelu.cz:
    ```
    ssh -D 5001 xjakubek@akela.mendelu.cz
    ```
    login with your credentials and do not this close terminal
2. run npm script:
    ```
    npm run tunnel
    ```
    You must have installed npx globally (`npm install -g npx`).

## Export collection
```
docker exec -it negracek-proquest-mongo bash
mongoexport --db proquest_db --collection test3 --out exported.json -u toso_admin -p Password123 --authenticationDatabase admin
```